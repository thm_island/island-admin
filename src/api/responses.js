class Responses {
    static DESCRIPTION_OK = 'OK';

    static DESCRIPTION_ERROR = 'An error has occurred.';

    static CODE_SUCCESS = 1;

    static CODE_ERROR = 2;

    static CODE_EXCEPTION = 3;

    static CODE_NOT_AUTHORIZED = 4;

    static CODE_VALIDATION_ERROR = 5;

    static CODE_USER_NOT_ASSIGNED = 6;

    static CODE_USER_EXISTS = 7;

    static successResponse = () => {
        const response = {
            status: {
                code: Responses.CODE_SUCCESS,
                description: Responses.DESCRIPTION_OK,
            },
        };
        return response;
    }

    static errorResponse = () => {
        const response = {
            status: {
                code: Responses.CODE_ERROR,
                description: Responses.DESCRIPTION_ERROR,
            },
        };
        return response;
    }
}

export default Responses;
