import StateService from '../super-simple-state'
import StateStores from '../state-stores'
import axios from 'axios'
import Responses from '../responses'

class AccountService
{
    static async authenticate(account)
    {
        let response = Responses.errorResponse();

        const url = process.env.REACT_APP_SERVICE_URL;
        console.log(url);
        
        try {
            const httpResponse = await axios.post(process.env.REACT_APP_SERVICE_URL + 'account/authenticate', account);
            const authResponse = httpResponse.data;
            
            if (authResponse.status.code === Responses.CODE_SUCCESS) {
                const nextAccountState = StateService.getCurrentState(StateStores.AccountStore);
                nextAccountState.currentSessionId = authResponse.sessionId;
                StateService.update(StateStores.AccountStore, nextAccountState);

                //serialize to local storage
                localStorage.setItem('currentSessionId', authResponse.sessionId);

                response = Responses.successResponse();
            }
            return response;
        }
        catch (error) {
            response.status.description = JSON.stringify(error);
            return response;
        }
    }

    static isAuthenticated()
    {
        let authenticated = false;
        const currentAccountState = StateService.getCurrentState(StateStores.AccountStore);
        if (currentAccountState.currentSessionId) {
            authenticated = true;
        }

        if(authenticated === false)
        {
            let sessionId = localStorage.getItem('currentSessionId');
            if(sessionId)
            {
                authenticated = true;
                //update state...
                const nextAccountState = StateService.getCurrentState(StateStores.AccountStore);
                nextAccountState.currentSessionId = sessionId;
                StateService.update(StateStores.AccountStore, nextAccountState);
            }
        }

        return authenticated;
    }
}

export default AccountService;