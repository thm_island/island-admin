import StateService from '../super-simple-state'
import StateStores from '../state-stores'
import axios from 'axios'
import Responses from '../responses'

class ProductService
{
    static async add(product)
    {
        let response = Responses.errorResponse();

        const url = process.env.REACT_APP_SERVICE_URL;
        console.log(url);
        
        try {
            const httpResponse = await axios.post(process.env.REACT_APP_SERVICE_URL + 'product/add', product);
            const authResponse = httpResponse.data;
            
            if (authResponse.status.code === Responses.CODE_SUCCESS) {
                const nextAccountState = StateService.getCurrentState(StateStores.AccountStore);
                nextAccountState.currentSessionId = authResponse.sessionId;
                StateService.update(StateStores.AccountStore, nextAccountState);
                response = Responses.successResponse();
            }
            return response;
        }
        catch (error) {
            response.status.description = JSON.stringify(error);
            return response;
        }
    }
}

export default AccountService;