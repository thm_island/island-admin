import * as THREE from 'three';
import StateService from '../super-simple-state';
import uuidv4 from 'uuid'

export default class Utils
{
    static normalizeToScale(val, a, b, c, d)
    {
        var newVal = 0.0;

        //Old Scale: a - b
        //New Scale: c - d

        newVal = c + (val - a) * (d - c) / (b - a);

        return newVal;
    }

    static radToDegClamped(rad)
    {
        var deg = THREE.Math.radToDeg(rad);
        deg %= 360;
        if (deg < 0)
        {
            deg = 360 + deg;
        }

        return deg;
    }

    static radToDeg(rad)
    {
        var deg = THREE.Math.radToDeg(rad);
        return deg;
    }

    static frameToTimecode(frame, addMillis = true)
    {
        const currentTimelineState = StateService.getCurrentState('TimelineStore');
        const duration = frame / currentTimelineState.fps;

        const secondsComponents = (duration + "").split(".");
        var secondsPart = secondsComponents[0];

        var millisceondsPart = ".0";
        if (secondsComponents.length > 1)
        {
            millisceondsPart = "." + secondsComponents[1];
        }
        const milliseconds = Math.round(parseFloat(millisceondsPart) * 1000);
        const totalMinutes = parseFloat(secondsPart) / 60.0;
        const minutesComponents = (totalMinutes + "").split(".");
        const minutes = parseInt(minutesComponents[0]);

        secondsPart = ".0";
        if (minutesComponents.length > 1)
        {
            secondsPart = "." + minutesComponents[1];
        }
        const seconds = Math.round(parseFloat(secondsPart) * 60);

        var timecode = Utils.pad2(minutes) + ":" + Utils.pad2(seconds);
        if(addMillis === true)
        {
            timecode += ":" + Utils.pad3(milliseconds);
        }

        return timecode;
    }

    static pad2(number)
    {
        return ("00" + number).slice(-2);
    }

    static pad3(number)
    {
        return ("000" + number).slice(-3);
    }

    static updateZoomLevel(zoomLevel)
    {
        var nextTimelineState = StateService.getCurrentState("TimelineStore");

        nextTimelineState.scrolledFrames *= zoomLevel;
        nextTimelineState.offsetFrame = nextTimelineState.scrolledFrames + (nextTimelineState.selectedFrame * zoomLevel);

        nextTimelineState.zoomLevel = zoomLevel;
        nextTimelineState.zoomLevelId = uuidv4();

        StateService.update("TimelineStore", nextTimelineState);
    }
}