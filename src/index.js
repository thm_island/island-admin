import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { BrowserRouter, Route } from 'react-router-dom';

ReactDOM.render(
    (<BrowserRouter>
        <div>
            <Route exact path='/index.html' component={App} key='0' />
            <Route exact path='/' component={App} key='1' />
        </div>
    </BrowserRouter>),
    document.getElementById('root'));
