import React, { Component } from 'react';
import Authentication from './Authentication/Authentication';
import Admin from './Admin/Admin';
import Account from './api/services/account';
import StateService from './api/super-simple-state';
import StateStores from './api/state-stores';

class App extends Component {

  componentDidMount()
  {
    StateService.subscribe(StateStores.AccountStore, (previousState, currentState) =>
    {
      if (previousState.currentSessionId !== currentState.currentSessionId)
      {
        this.setState({refresh: new Date()});
      }
    });
  }

  render() {

    let isAuthenticated = Account.isAuthenticated();

    return (
      <div>
        THM Admin
        {!isAuthenticated && <Authentication />}
        {isAuthenticated && <Admin />}
      </div>
    );
  }
}

export default App;
