import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import StateService from '../api/super-simple-state';
import StateStores from '../api/state-stores';
import AccountService from '../api/services/account'

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

export default function Authentication() {

    const classes = useStyles();

    const [ username, setUsername ] = useState('');
    const [ password, setPassword ] = useState('');

    const handleAuthentication = event => 
    {
        const account = {
            user:
            {
                username,
                password,
            }
        };
        AccountService.authenticate(account);
    }

    const handleUsernameChange = event => setUsername(event.target.value);
    const handlePasswordChange = event => setPassword(event.target.value);

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <div>
                <TextField
                    required
                    id="outlined-required"
                    label="username"
                    variant="outlined"
                    value={username}
                    onChange={handleUsernameChange}
                />
            </div>
            <div>
                <TextField
                    required
                    id="outlined-required"
                    label="password"
                    variant="outlined"
                    value={password}
                    onChange={handlePasswordChange}
                />
            </div>
            <div>
                <Button onClick={handleAuthentication} variant="contained" size="large" color="primary" className={classes.margin}>
                    Go
                </Button>
            </div>
        </form>
    );
}